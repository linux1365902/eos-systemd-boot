# kernel-install-mkinitcpio

A framework for enabling systemd-boot automation using kernel-install with mkinitcpio on Arch Linux

The package does a few things:
* Overrides the mkinitcpio hooks to generate presets that work with kernel-install
* Installs hooks to automate the installation and removal of kernels using kernel-install
* Adds support for generating fallback images to kernel-install
* Saves kernel options to /etc/kernel/cmdline to support recovery in a chroot

